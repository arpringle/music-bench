# Music Bench

Make music, effortlessly.

In layman's terms, *Music Bench* is a music production tool that is intended to
be easy, fast, and beautiful.

For the technical folks, *Music Bench* is a cross-platform, free-and-open-source
digital audio workstation software, written in Rust and utilizing the GNOME
software stack, i.e., gtk4 & libadwaita. 

Or at least, that's what I would say if this was a functional thing.

This repo is really just me learning the GNOME stack and also rust.
