pub static VERSION: &str = "0.1.0";
pub static GETTEXT_PACKAGE: &str = "music_bench";
pub static LOCALEDIR: &str = "/app/share/locale";
pub static PKGDATADIR: &str = "/app/share/music_bench";
